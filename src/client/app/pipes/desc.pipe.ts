import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'desc'})
export class DescPipe implements PipeTransform {
  transform(list: any[], filterByField: string, filterValue: string): any {
    //return list;
    if (!filterByField || ! filterValue || filterValue === '*') {
      return list;
    }

    return list.filter(item => {
      const field = item[filterByField].toLowerCase();
      const filter = filterValue.toLocaleLowerCase();
      return field.indexOf(filter) >= 0;
    });
  }
}
