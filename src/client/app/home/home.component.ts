import { Component, OnInit, Input } from '@angular/core';
import { BroadcastListService } from '../shared/broadcast-list/broadcast-list.service';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css']
})

export class HomeComponent implements OnInit {
  @Input() channelFilter: string = '*';
  @Input() keywordFilter: string = '';
  newBroadcast: string = '';
  errorMessage: string;
  broadcasts: any[] = [];

  /**
   * Creates an instance of the HomeComponent with the injected
   * BroadcastListService.
   *
   * @param {BroadcastListService} broadcastListService - The injected BroadcastListService.
   */
  constructor(public broadcastListService: BroadcastListService) {}

  /**
   * Get the broadcasts OnInit
   */
  ngOnInit() {
    this.getBroadcasts();
  }

  /**
   * Handle the broadcastListService observable
   */
  getBroadcasts() {
    this.broadcastListService.get()
      .subscribe(
        broadcasts => this.broadcasts = broadcasts,
        error => this.errorMessage = <any>error
      );
  }

  /**
   * Pushes a new broadcast onto the broadcasts array
   * @return {boolean} false to prevent default form submit behavior to refresh the page.
   */
  addBroadcast(): boolean {
    // TODO: implement broadcastListService.post
    this.broadcasts.push(this.newBroadcast);
    this.newBroadcast = '';
    return false;
  }

}
