import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ToolbarComponent } from './toolbar/toolbar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BroadcastListService } from './broadcast-list/broadcast-list.service';
import { ChannelPipe } from '../pipes/channel.pipe';
import { DescPipe } from '../pipes/desc.pipe';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [ToolbarComponent, NavbarComponent, ChannelPipe, DescPipe],
  exports: [ToolbarComponent, NavbarComponent,
    CommonModule, FormsModule, RouterModule, ChannelPipe, DescPipe]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [BroadcastListService]
    };
  }
}
